//
//  DO NOT MANUAL EDIT THIS FILE!
//  generated from 'chip/integration/sysdef.xls' by 'chip/integration/sysdef/sysdef.pl'
//

#ifndef HW_DMA_DEFS
#define HW_DMA_DEFS

// APBH DMA CHANNEL ASSIGNMENTS
#define                 HW_APBH_DMA_SSP0_CHANNEL (  0 )
#define                 HW_APBH_DMA_SSP1_CHANNEL (  1 )
#define                 HW_APBH_DMA_SSP2_CHANNEL (  2 )
#define                 HW_APBH_DMA_SSP3_CHANNEL (  3 )
#define             HW_APBH_DMA_GPMI_CH0_CHANNEL (  4 )
#define             HW_APBH_DMA_GPMI_CH1_CHANNEL (  5 )
#define             HW_APBH_DMA_GPMI_CH2_CHANNEL (  6 )
#define             HW_APBH_DMA_GPMI_CH3_CHANNEL (  7 )
#define             HW_APBH_DMA_GPMI_CH4_CHANNEL (  8 )
#define             HW_APBH_DMA_GPMI_CH5_CHANNEL (  9 )
#define             HW_APBH_DMA_GPMI_CH6_CHANNEL ( 10 )
#define             HW_APBH_DMA_GPMI_CH7_CHANNEL ( 11 )
#define                HW_APBH_DMA_HSADC_CHANNEL ( 12 )
#define                HW_APBH_DMA_LCDIF_CHANNEL ( 13 )

// APBX DMA CHANNEL ASSIGNMENTS
#define            HW_APBX_DMA_UART4_RX_CHANNEL  (  0 )
#define            HW_APBX_DMA_UART4_TX_CHANNEL  (  1 )
#define                HW_APBX_DMA_SPDIF_CHANNEL (  2 )
#define                HW_APBX_DMA_SAIF0_CHANNEL (  4 )
#define                HW_APBX_DMA_SAIF1_CHANNEL (  5 )
#define                 HW_APBX_DMA_I2C0_CHANNEL (  6 )
#define                 HW_APBX_DMA_I2C1_CHANNEL (  7 )
#define            HW_APBX_DMA_UART0_RX_CHANNEL  (  8 )
#define            HW_APBX_DMA_UART0_TX_CHANNEL  (  9 )
#define            HW_APBX_DMA_UART1_RX_CHANNEL  ( 10 )
#define            HW_APBX_DMA_UART1_TX_CHANNEL  ( 11 )
#define            HW_APBX_DMA_UART2_RX_CHANNEL  ( 12 )
#define            HW_APBX_DMA_UART2_TX_CHANNEL  ( 13 )
#define            HW_APBX_DMA_UART3_RX_CHANNEL  ( 14 )
#define            HW_APBX_DMA_UART3_TX_CHANNEL  ( 15 )

// legacy definition
#define HW_APBH_DMA_NAND_CS0_CHANNEL (  4 )
#define HW_APBH_DMA_NAND_CS1_CHANNEL (  5 )
#define HW_APBH_DMA_NAND_CS2_CHANNEL (  6 )
#define HW_APBH_DMA_NAND_CS3_CHANNEL (  7 )
#define HW_APBH_DMA_NAND_CS4_CHANNEL (  8 )
#define HW_APBH_DMA_NAND_CS5_CHANNEL (  9 )
#define HW_APBH_DMA_NAND_CS6_CHANNEL ( 10 )
#define HW_APBH_DMA_NAND_CS7_CHANNEL ( 11 )

#define HW_APBX_DMA_SPDIF_TX_CHANNEL (  2 )
#endif // HW_DMA_DEFS
